import React from "react";
import HeaderTheme from "../pages/components/HeaderTheme";
import Footer from "./../pages/HomePage/Footer";

export default function Layout({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
      <Footer />
    </div>
  );
}
