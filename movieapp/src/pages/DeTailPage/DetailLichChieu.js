import moment from "moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function DetailLichChieu({ dataLichChieu }) {
  console.log(dataLichChieu);

  return (
    <div className="text-white ">
      <div>
        {dataLichChieu.lstLichChieuTheoPhim
          .splice(0, 1)
          .map((lichChieu, index) => {
            return (
              <div key={index} className="flex grid-cols-3">
                <NavLink to={`/checkout/${lichChieu.maLichChieu}`}>
                  <div
                    className="bg-red-500 mx-10  text-white my-2 p-2 rounded"
                    key={index}
                  >
                    <p>
                      {moment(lichChieu.ngayChieuGioChieu).format("DD-MM_YYYY")}
                    </p>
                    <span className="text-yellow-500 font-medium">
                      {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                    </span>
                  </div>
                </NavLink>
              </div>
            );
          })}
      </div>
    </div>
  );
}
