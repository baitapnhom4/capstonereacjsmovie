import { Progress, Rate } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieService } from "../services/movieService";
import { Tabs } from "antd";

import DetailLichChieu from "./DetailLichChieu";

const { TabPane } = Tabs;
export default function DeTailPage() {
  let { id } = useParams();

  const [movie, setMovie] = useState({});
  const [dataPhim, setDataPhim] = useState([]);
  // console.log("dataPhim", dataPhim);
  // console.log("movie", movie);

  useEffect(() => {
    movieService
      .getMovieDetail(id)
      .then((res) => {
        // console.log(res.data);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [id]);
  useEffect(() => {
    movieService
      .getMovieListbyTheater()
      .then((res) => {
        // console.log(res.data.content);
        setDataPhim(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderContentRap = () => {
    return dataPhim.map((heThongRap, index) => {
      return (
        <TabPane
          style={{ height: 500 }}
          tab={<img className="w-16" src={heThongRap.logo} />}
          key={index}
          className="overflow-y-scroll scrollbar-thin scrollbar-thumb-red-500 scrollbar-track-green-600  "
        >
          {heThongRap.lstCumRap.splice(0, 2).map((cumRap) => {
            return (
              <div key={cumRap.maCumRap} className="   shadow-slate-100 ">
                <div className="text-left ">
                  <p className="text-red-500 py-2 text-xl">
                    {cumRap.tenCumRap}
                  </p>
                  <p className="text-green-500">{cumRap.diaChi}</p>
                </div>
                <div className="flex " style={{ height: 200 }}>
                  {cumRap.danhSachPhim.map((phim, index) => {
                    return <DetailLichChieu dataLichChieu={phim} key={index} />;
                  })}
                </div>
              </div>
            );
          })}
        </TabPane>
      );
    });
  };
  // let renderTenCumRap = (cumRap) => {
  //   return (

  //   );
  // };
  return (
    <div className=" mx-auto   ">
      <div
        style={{
          width: "100%",
          height: "100%",
          backgroundImage: `url(https://tse3.mm.bing.net/th?id=OIP.peY_TbfWf8IxrUEUaCGQPQHaEK&pid=Api&P=0)`,
        }}
        className=" text-white  mx-auto bg-no-repeat  bg-cover "
      >
        <div className="flex  justify-between items-center p-20 space-x-10  container mx-auto">
          <div className="flex mx-auto" style={{ width: "75%" }}>
            <img
              style={{ width: 400, height: 500 }}
              src={movie.hinhAnh}
              alt=""
              className="w-80 rounded-2xl mt-5 "
            />
            <div className="px-10 text-base font-medium">
              <p className="my-10">
                Tên Phim :<span className=" text-xl px-2">{movie.tenPhim}</span>
              </p>

              <p className="my-10">
                Nội Dung Phim :
                <span className="px-2 font-normal">{movie.moTa}</span>
              </p>
              <div className="py-5">
                <Rate value={movie.danhGia / 2} />
              </div>
              <a href={movie.trailer}>
                <button className="px-3 py-2 bg-slate-500 text-white rounded hover:bg-gray-400 duration-500 ">
                  Trailler
                </button>
              </a>
              <button className="mx-5 px-3 py-2 bg-blue-800 text-white rounded  hover:bg-blue-600 duration-500">
                Xem Phim
              </button>
            </div>
          </div>
          <div className="text-center font-medium  " style={{ width: "25%" }}>
            <p className="text-2xl py-5 text-blue-200 ">Điểm Đánh Giá</p>
            <Progress
              type="circle"
              percent={movie.danhGia * 10}
              format={(number) => {
                return (
                  <span className="text-blue-400 font-medium text-center ">
                    {number / 10} Điểm
                  </span>
                );
              }}
            />
            <p className=" py-20 ">
              <a href="#lichchieu" alt>
                <button className="px-4 py-2 rounded bg-red-500 text-white  text-2xl hover:text-3xl duration-500">
                  Đặt Vé Now
                </button>
              </a>
            </p>
          </div>
        </div>
        <hr
          className="py-1 mx-auto my-5 bg-gray-500 border-gray-500 rounded "
          style={{ width: "80%" }}
        />
        <div
          id="lichchieu"
          className=" container mx-auto  text-white bg-gray-900 bg-opacity-90 rounded "
        >
          <Tabs tabPosition="left" defaultActiveKey="1">
            {renderContentRap()}
          </Tabs>
        </div>
      </div>
    </div>
  );
}
