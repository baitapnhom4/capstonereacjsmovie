import React, { useState, useEffect, Fragment } from "react";
import { movieService } from "../services/movieService";
import "./Checkout.css";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import { layChiTietPhongVeAction } from "../redux/actions/GetDetailPhongVeAction";
import { CloseOutlined } from "@ant-design/icons";

export default function Checkout() {
  const [dataPhim, setDataPhim] = useState([]);
  let { id } = useParams();
  const dispatch = useDispatch();
  const { detailTheater } = useSelector((item) => {
    return item.movieReducer;
  });

  let { thongTinPhim, danhSachGhe } = detailTheater;
  console.log(danhSachGhe);
  useEffect(() => {
    dispatch(layChiTietPhongVeAction(id));
  }, []);

  console.log("maLichChieu", id);
  useEffect(() => {
    movieService
      .getMovieListbyTheater()
      .then((res) => {
        // console.log(res.data.content);
        setDataPhim(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderDanhSachGhe = () => {
    return danhSachGhe?.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      return (
        <Fragment key={index}>
          <button
            disabled={ghe.daDat}
            className={`ghe ${classGheVip} ${classGheDaDat}`}
          >
            {ghe.daDat === true ? (
              <CloseOutlined style={{ marginBottom: "7px" }} />
            ) : (
              ghe.stt
            )}
          </button>
          {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };
  return (
    <div className="container pt-20 mx-auto   ">
      <div className="grid grid-cols-12 font-medium gap-10 ">
        <div className="col-span-8 ">
          <div className=" container mx-auto py-5 ">
            <div className="bg-black h-4 mb-5"></div>
            <div className="bg-gray-300 h-10 text-2xl text-white text-center my-5">
              Màn Hình
            </div>
            <div className="container mx-auto items-center ">
              {renderDanhSachGhe()}
            </div>
          </div>
        </div>
        <div className="col-span-4" style={{ fontSize: 18 }}>
          <h3 className="text-green-500 text-center " style={{ fontSize: 40 }}>
            0 VND
          </h3>
          <hr />
          <p className="py-5">
            Cụm Rạp: <span className="mx-10">FUCKERelo</span>
          </p>
          <hr />
          <p className="py-5">
            Địa chỉ: <span className="mx-10">FUCKERelo</span>
          </p>

          <hr />
          <p className="py-5">
            Rạp: <span className="mx-10">FUCKERelo</span>
          </p>

          <hr />
          <p className="py-5">
            Ngày giờ chiếu: <span className="mx-10">FUCKERelo</span>
          </p>

          <hr />
          <p className="py-5">
            Tên Phim: <span className="mx-10">FUCKERelo</span>
          </p>

          <hr />
          <div className="flex grid-cols-2">
            <div className="col-span-8">
              <span className="text-red-500">Ghế :</span>
            </div>
            <div className="col-span-4 px-10">0 VND</div>
          </div>
        </div>
      </div>
    </div>
  );
}
