import { Carousel } from "flowbite-react";

import React from "react";
import { useSelector } from "react-redux";

export default function Carosel() {
  const { arrCarousel } = useSelector((item) => {
    return item.userReducer;
  });
  // console.log(arrCarousel);

  let renderCarousel = () => {
    return arrCarousel.map((item) => {
      return <img className="w-screen h-screen  " src={item.hinhAnh} alt="" />;
    });
  };
  return (
    <div className="h-72 md:h-[40vh] xl:h-[80vh] 2xl:h-screen">
      <Carousel slideInterval={5000}>{renderCarousel()}</Carousel>
    </div>
  );
}
