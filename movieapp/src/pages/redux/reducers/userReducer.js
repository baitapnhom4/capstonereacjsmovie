import { localStorageSer } from "../../services/localStoreageService";
import { arrCarousel } from "./reducerCarousel";

export const LOGIN = "LOGIN";
export const REGISTER = "REGISTER";
let initialState = {
  userInfor: localStorageSer.user.get(),
  registerData: null,
  arrCarousel,
};
export let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN: {
      state.userInfor = action.payload;
      return { ...state };
    }
    case REGISTER: {
      state.registerData = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
