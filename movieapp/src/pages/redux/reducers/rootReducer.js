import { combineReducers } from "redux";
import { movieReducer } from "./movieReducer";

import { userReducer } from "./userReducer";

export let rootReducer = combineReducers({
  userReducer,
  movieReducer,
});
