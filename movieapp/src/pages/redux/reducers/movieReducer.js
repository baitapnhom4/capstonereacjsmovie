import { ThongTinLichChieu } from "../../../models/ThongTinPhongVeController";
import { SET_CHI_TIET_PHONG_VE } from "../../services/cogfigURL";
import { SET_MOVIE_LIST } from "../constants/movieConts";

let initialState = {
  movieList: [],
  detailTheater: new ThongTinLichChieu(),
};
// rxreducer
export let movieReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_MOVIE_LIST:
      state.movieList = payload;
      return { ...state };
    case SET_CHI_TIET_PHONG_VE:
      state.detailTheater = payload;
      return { ...state };
    default:
      return state;
  }
};
