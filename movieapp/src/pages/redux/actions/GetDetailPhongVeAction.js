import { SET_CHI_TIET_PHONG_VE } from "../../services/cogfigURL";
import { movieService } from "../../services/movieService";

export const layChiTietPhongVeAction = (maLichChieu) => {
  return async (dispatch) => {
    await movieService
      .getDetailTheater(maLichChieu)
      .then((res) => {
        dispatch({
          type: SET_CHI_TIET_PHONG_VE,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  //   type: second,
  //   payload
};
