// import { useDispatch } from "react-redux";
import { movieService } from "../../services/movieService";
import { SET_MOVIE_LIST } from "../constants/movieConts";

export const getmovieListActionSer = () => {
  return (dispatch) => {
    movieService
      .getMovieList()
      .then((res) => {
        dispatch({
          type: SET_MOVIE_LIST,
          payload: res.data.content,
        });
        // console.log(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
