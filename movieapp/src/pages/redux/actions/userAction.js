import { LOGIN, REGISTER } from "../reducers/userReducer";

export let loginAction = (dataLogin) => {
  return {
    type: LOGIN,
    payload: dataLogin,
  };
};
export let registerAction = (registerInform) => {
  return {
    type: REGISTER,
    payload: registerInform,
  };
};
