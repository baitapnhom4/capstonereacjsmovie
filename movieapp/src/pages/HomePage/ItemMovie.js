import React from "react";
import moment from "moment";
import { NavLink } from "react-router-dom";
export default function ItemMovie({ phim }) {
  // console.log(phim);
  return (
    <div className="flex">
      <NavLink to={`detail/${phim.maPhim}`}>
        <img
          style={{ width: 100, height: 150 }}
          className=" py-3 object-cover  rounded "
          src={phim.hinhAnh}
          alt=""
        />
      </NavLink>

      <div>
        <p className="font-medium text-2xl text-yellow-500 m-5">
          {phim.tenPhim}
        </p>
        <div className="grid grid-cols-4 gap-5 mx-2">
          {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu, index) => {
            return (
              <NavLink key={index} to={`checkout/${lichChieu.maLichChieu}`}>
                <div className="bg-red-700 text-white p-2 rounded ">
                  <p>
                    {moment(lichChieu.ngayChieuGioChieu).format("DD-MM_YYYY")}
                  </p>
                  <span className="text-yellow-400 font-medium">
                    {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                  </span>
                </div>
              </NavLink>
            );
          })}
        </div>
      </div>
    </div>
  );
}
