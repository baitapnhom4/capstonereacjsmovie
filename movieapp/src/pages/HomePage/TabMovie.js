import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../services/movieService";
import ItemMovie from "./ItemMovie";

const { TabPane } = Tabs;
export default function TabMovie() {
  let [dataMovie, setDataMovie] = useState([]);
  // console.log("dataMovie", dataMovie);
  const onChange = (key) => {
    console.log(key);
  };
  useEffect(() => {
    movieService
      .getMovieListbyTheater()
      .then((res) => {
        // console.log(res.data.content);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContentRap = () => {
    return dataMovie.map((heThongRap, index) => {
      return (
        <TabPane
          style={{ height: 600 }}
          tab={<img className="w-16" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs tabPosition="left" defaultActiveKey="1" style={{ height: 600 }}>
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <TabPane
                  tab={renderTenCumRap(cumRap)}
                  key={cumRap.maCumRap}
                  onChange={onChange}
                  className="scrollbar-thin scrollbar-thumb-blue-700 scrollbar-track-blue-300  overflow-y-scroll shadow-md"
                >
                  <div style={{ height: 600 }}>
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemMovie phim={phim} key={phim.maPhim} />;
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  let renderTenCumRap = (cumRap) => {
    return (
      <div className="text-left" style={{ width: 300 }}>
        <p className="text-white">{cumRap.tenCumRap}</p>
        <p className="text-green-500">{cumRap.diaChi}</p>
        <button className="text-red-500">[Xem Chi Tiết]...</button>
      </div>
    );
  };
  return (
    <div className="container mx-auto py-5 text-white bg-gray-800 bg-opacity-70 rounded">
      <div className="py-5" id="lichchieu">
        <p className="text-3xl font-medium px-5">
          DANH SÁCH HỆ THỐNG RẠP & LỊCH CHIẾU
        </p>
      </div>
      <div>
        <Tabs tabPosition="left" defaultActiveKey="1">
          {renderContentRap()}
        </Tabs>
      </div>
    </div>
  );
}
