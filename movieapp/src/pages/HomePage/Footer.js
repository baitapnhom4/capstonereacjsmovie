import React, { useEffect, useState } from "react";
import { movieService } from "./../services/movieService";
// import { NavLink } from "react-router-dom";

export default function Footer() {
  const [dataLogo, setDataLogo] = useState([]);

  useEffect(() => {
    movieService
      .getMovieListbyTheater()
      .then((res) => {
        // console.log(res.data.content);
        setDataLogo(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderLogoDoiTac = () => {
    return dataLogo.map((item, index) => {
      return (
        <div key={index} className="my-3 flex justify-center items-center">
          <ul>
            <li>
              <a href="">
                {" "}
                <img className="w8- h-8" src={item.logo} alt="" />
              </a>
            </li>
          </ul>
        </div>
      );
    });
  };

  let renderContentFooter = () => {
    return (
      <div>
        <div className="container mx-auto text-white flex justify-evenly  text-center  ">
          <div className="">
            <p className="font-medium py-5">TIX</p>

            <div className="flex justify-evenly mx-2 ">
              <div className="text-gray-400 m-5">
                <p className=" hover:text-white my-3">FAQ</p>
                <p className=" hover:text-white">Brand Guidelines</p>
              </div>
              <div className="text-gray-400 m-5 ">
                <p className=" hover:text-white my-3"> Thỏa thuận sử dụng</p>
                <p className=" hover:text-white">Chính sách bảo mật</p>
              </div>
            </div>
          </div>
          <div>
            <p className="font-medium py-5">ĐỐI TÁC</p>
            <div className="grid grid-cols-4 gap-10">{renderLogoDoiTac()}</div>
          </div>
          <div className="flex font-medium py-5 ">
            <div className="mx-3">
              <p className="my-4">MOBIE APP</p>
              <div className="flex ">
                <a href="" className="">
                  <img className="w-6 m-2 " src="./img2/apple.png" alt="" />
                </a>
                <a href="https://play.google.com/store/apps?gl=us">
                  <img className="w-6 m-2" src="./img2/android.png" alt="" />
                </a>
              </div>
            </div>
            <div>
              <p className="my-4">SOCIAL</p>
              <div className="flex">
                <a href="https://www.facebook.com/duytan.nguyen.102/">
                  <img className="w-8 m-2" src="./img2/facebook.png" alt="" />
                </a>
                <a href="https://www.instagram.com/tan_96_nguyen/">
                  <img className="w-8 m-2" src="./img2/zalo.png" alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
        <hr style={{ width: "60%" }} className="mx-auto pb-3 " />
        <div className="flex justify-center text-center items-center">
          <div>
            <img className="w-20" src="./img2/logo1.jpg" alt="" />
          </div>
          <div className="text-left px-3 mx-3 text-white">
            <p>TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</p>
            <p>
              Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ
              Chí Minh, Việt Nam.
            </p>
            <p>Giấy chứng nhận đăng ký kinh doanh số: 01234567893,</p>
            <p>
              đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế
              hoạch và đầu tư Thành phố Hồ Chí Minh cấp.
            </p>
            <p>Số Điện Thoại (Hotline): +84 38222 1158</p>
          </div>
          <div>
            <img className="w-32" src="./img2/logodathongbao.png" alt="" />
          </div>
        </div>
      </div>
    );
  };
  return <div className="bg-gray-700  py-5">{renderContentFooter()}</div>;
}
