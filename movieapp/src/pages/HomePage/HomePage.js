import React from "react";
import { UpCircleOutlined } from "@ant-design/icons";
import Carosel from "../Carosel/Carosel";
// import Footer from "./Footer";
import ListMovie from "./ListMovie";
import TabMovie from "./TabMovie";
import UngDung from "./UngDung";
const btnSrollToTop = document.querySelector("#scrollToTop");
let clickGotoTop = () => {
  btnSrollToTop(
    window.scrollTo({
      top: 0,
      left: 0,
    })
  );
};

export default function HomePage() {
  return (
    <div className="w-full">
      <div className="bg-black relative ">
        <Carosel />
        <ListMovie />
        <TabMovie />
        <UngDung />
        {/* <Footer /> */}
      </div>
      <div className="absolute">
        <button
          onClick={clickGotoTop}
          id="scrollToTop"
          style={{ bottom: 50, right: 50 }}
          className="bg-yellow-500 bg-opacity-90 text-white  rounded-2xl p-2 transition- hover:translate-y-2 fixed font-medium"
        >
          <span>
            GO T
            <UpCircleOutlined className=" text-red-500 font-medium text-xl" />
            TOP
          </span>
        </button>
      </div>
    </div>
  );
}
