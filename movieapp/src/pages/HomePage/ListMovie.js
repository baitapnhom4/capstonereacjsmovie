import React, { useEffect, useState } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useDispatch, useSelector } from "react-redux";
import { getmovieListActionSer } from "../redux/actions/movieAction";
import { NavLink } from "react-router-dom";
import { Card, Rate } from "antd";

const { Meta } = Card;

export default function ListMovie() {
  let dispatch = useDispatch();
  // Open and Close listItem
  const [isData, setData] = useState(true);
  // console.log(isData);
  // lấy thông tin từ Store
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });

  useEffect(() => {
    dispatch(getmovieListActionSer());
  }, []);

  let renderMovieItem = () => {
    return movieList
      .filter((item) => {
        return item !== null;
      })
      .map((item) => {
        return (
          <Card
            key={item.maPhim}
            hoverable
            style={{
              height: "auto",
              borderRadius: 15,
            
            }}
            className="shadow-xl hover:shadow-2xl hover:shadow-white transition-all hover:translate-y-3  bg-gray-800 border-black grid-cols-1 mx-1 sm:mx-2 md:mx-2 lg:mx-2 xl:mx-3 2xl:mx-5"
            cover={
              <img
                style={{
                  // height: 400,
                  borderRadius: 15,
                  borderBottomRightRadius: 0,
                  borderBottomLeftRadius: 0,
                }}
                alt="example"
                src={item.hinhAnh}
                className="h-52 w-full md:h-56 lg:h-72"
              />
            }
          >
            <Meta
              title={
                <div>
                  <span className="text-yellow-500">{item.tenPhim}</span>
                </div>
              }
              description={
                <p className="text-white">
                  <Rate value={item.danhGia / 2} disabled />
                </p>
              }
              className="mb-3 "
              style={{ height: 60 }}
            />

            <NavLink to={`detail/${item.maPhim}`}>
              <button className="w-full bg-red-700 text-white px-5 py-2 my-1 rounded hover:bg-red-500 ">
                Mua Vé
              </button>
            </NavLink>
          </Card>
        );
      });
  };
  let handleBindingListMovie = (e) => {
    setData(e);
  };
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1581 },
      items: 5,
    
    },
    desktop1: {
      breakpoint: { max: 1580, min: 1024 },
      items: 5,
    
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
    
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2,
    
    }
  };
  return (
    <div className=" container mx-auto ">
      <div className="text-right py-1 sm:py-2 xl:py-5 lg:py-3  mx-auto ">
        {isData ? (
          <button
            className="text-white text-xl   "
            onClick={() => {
              handleBindingListMovie(false);
            }}
          >
            {"Xem tất cả >>"}
          </button>
        ) : (
          <button
            className="text-white text-xl   "
            onClick={() => {
              handleBindingListMovie(true);
            }}
          >
            {"Rút gọn <<"}
          </button>
        )}
      </div>
      <div className=" ">
        {isData ? (
          <Carousel
  swipeable={false}
  draggable={false}
  // showDots={true}
  responsive={responsive}
  ssr={true} // means to render carousel on server-side.
  slidesToSlide={2}
  infinite={true}
  // autoPlay={this.props.deviceType !== 'mobile' ? true : false}
  // autoPlaySpeed={1000}
  keyBoardControl={true}
  customTransition='all .5'
  transitionDuration={500}
  containerClass='carousel-container'
  // removeArrowOnDeviceType={['tablet', 'mobile']}
  // deviceType={this.props.deviceType}
  dotListClass='custom-dot-list-style'
  itemClass='carousel-item-padding-40-px'
  className="pb-5"
>
            {renderMovieItem()}
          </Carousel>
        ) : (
          <div className="grid grid-cols-2 2xl:grid-cols-5 xl:grid-cols-5 gap-5 lg:grid-cols-4 sm:grid-cols-2 container mx-auto py-5 ">
            {renderMovieItem()}
          </div>
        )}
      </div>
    </div>
  );
}
