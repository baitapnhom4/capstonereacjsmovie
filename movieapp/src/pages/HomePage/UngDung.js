import React from "react";
// import { Carousel } from "antd";

export default function UngDung() {
  let renderUngDUng = () => {
    return (
      <div
        className=" w-full h-full  bg-contain mt-5"
        style={{ backgroundImage: "url(./img/backapp.jpg)" }}
      >
        <div className="container mx-auto text-white flex justify-center items-center">
          <div className="px-10 mx-20 font-medium items-center">
            <p className="text-2xl  my-10">
              Ứng dụng tiện lợi dành cho người yêu điện ảnh
            </p>

            <p className="py-5">
              Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và
              đổi quà hấp dẫn.
            </p>
            <a href="https://www.apple.com/app-store/ " target="_blank">
              <button className="px-4 py-2 bg-orange-700 rounded my-5 text-white  hover:bg-orange-500 ">
                APP MIỄN PHÍ - TẢI VỀ NGAY
              </button>
            </a>

            <p>
              TIX có hai phiên bản
              <a href="https://www.apple.com/app-store/">
                <span className="underline"> IOS </span>
              </a>
              &
              <a href="https://play.google.com/store/apps?gl=us">
                <span className="underline "> Android </span>
              </a>
            </p>
          </div>
          <div className=" px-10 py-10 items-center ">
            <div className=" absolute ">
              <img style={{ width: 200 }} src="./img/screenPhone.png" alt="" />
            </div>

            <div className="relative top-3 left-2 " style={{ width: "100%" }}>
              <a
                href="https://www.youtube.com/watch?v=c9rrThkwnEY"
                target="_blank"
              >
                <img
                  src="./img2/banner1.jpg"
                  alt=""
                  style={{ width: 186 }}
                  className="overflow-hidden rounded-[17px] "
                />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  };
  return <div>{renderUngDUng()}</div>;
}
