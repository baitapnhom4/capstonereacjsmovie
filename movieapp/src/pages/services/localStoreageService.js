export const USERS = "USERS";
export let localStorageSer = {
  user: {
    // dữ liệu đúng thì lưu vào localStorage
    set: function (dataUser) {
      let dataJson = JSON.stringify(dataUser);
      localStorage.setItem(USERS, dataJson);
    },
    // lấy dư liệu từ localStorage gáng vào redux userInfor
    get: function () {
      let dataJson = localStorage.getItem(USERS);
      if (!dataJson) {
        return null;
      } else {
        return JSON.parse(dataJson);
      }
    },
    remove: function () {
      localStorage.removeItem(USERS);
    },
  },
};

// sesstionStorage :  lưu LocalStorage sử dụng ,khi tắt máy bật lại yêu cầu đăng nhập
