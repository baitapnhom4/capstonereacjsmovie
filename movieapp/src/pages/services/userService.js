import { https } from "./cogfigURL";

export let userSevice = {
  postLogin: (loginData) => {
    // axios lưu thông tin đăng nhập
    // return axios({
    //   url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
    //   method: "POST",
    //   data: loginData,
    //   headers: {
    //     TokenCybersoft: TOKEN_CYBERSOFT,
    //   },
    // });
    return https.post("/api/QuanLyNguoiDung/DangNhap", loginData);
  },

  postRegister: (dataRegister) => {
    return https.post("/api/QuanLyNguoiDung/DangKy", dataRegister);
  },
};
