import React from "react";
import { Form, Input, message } from "antd";
import LoginAnimate from "./LoginAnimate";
import { userSevice } from "../services/userService";
import { localStorageSer } from "../services/localStoreageService";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginAction } from "../redux/actions/userAction";
export default function LoginPage() {
  // useDispatch :lấy thông tin lưu vào state trong redux
  let dispatch = useDispatch();
  // useNavigate dùng để chuyển trang
  let history = useNavigate();

  const onFinish = (values) => {
    console.log("Success:", values);
    userSevice
      .postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");

        console.log(res);
        localStorageSer.user.set(res.data.content);

        dispatch(loginAction(res.data.content));
        // đăng nhập thành công lưu localStorage & Chuyển trang
        // window.location.href = "/";
        // setTimeout: để thời gian chuyển trang

        setTimeout(() => {
          history("/");
        }, [1000]);
      })

      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err?.response?.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg-blue-200 h-screen w-screen p-20">
      <div
        className="container   mx-auto bg-red-300  rounded-xl p-10 "
        style={{ width: 700 }}
      >
        <NavLink to="/">
          <p className="text-2xl font-medium text-red-700">HD MOVIES</p>
        </NavLink>
        <div>
          <p className="font-medium text-3xl text-center py-5 text-blue-500">
            ĐĂNG NHẬP
          </p>
        </div>
        <div className="flex">
          <div className="w-1/2 py-10">
            <Form
              name="basic"
              layout="vertical"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 24,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Username"
                name="taiKhoan"
                style={{ colors: "white" }}
                rules={[
                  {
                    required: true,
                    message: "Please input your username!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="matKhau"
                style={{ colors: "white" }}
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <div className="flex justify-center">
                <button className="shadow py-2 px-5 rounded bg-blue-500  text-white hover:shadow-2xl mx-3">
                  Đăng Nhập
                </button>
                <NavLink to="/register">
                  <button className="shadow py-2 px-5 rounded bg-green-700  text-white hover:shadow-2xl">
                    Đăng Ký
                  </button>
                </NavLink>
              </div>
            </Form>
          </div>
          <div className="w-1/2 h-96 ">
            <LoginAnimate />
          </div>
        </div>
      </div>
    </div>
  );
}
