import React from "react";
import Lottie from "lottie-react";
import bgAnimate from "../../assets/AnimateLogin.json";
export default function LoginAnimate() {
  return (
    <div className="transform -translate-y-130">
      <Lottie animationData={bgAnimate} />
    </div>
  );
}
