import React from "react";
import { Form, Input, message } from "antd";
import { userSevice } from "../services/userService";
import { useDispatch } from "react-redux";
import { registerAction } from "../redux/actions/userAction";
import { NavLink, useNavigate } from "react-router-dom";
export default function RegisterPage() {
  const [form] = Form.useForm();
  // UseDispatch Lấy dữ liệu từ form lưu vào state redux
  let dispatch = useDispatch();
  let history = useNavigate();
  const onFinish = (values) => {
    console.log("Received values of form: ", values);

    userSevice
      .postRegister(values)
      .then((res) => {
        console.log(res.data);
        message.success("Đăng ký thành công");
        dispatch(registerAction(res.data.content));
        setTimeout(() => {
          history("/login");
        }, [2000]);
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };

  return (
    <div className="bg-black w-screen h-screen py-20 bg-scale-500">
      <div className="container mx-auto bg-gray-400 py-5 rounded-2xl max-w-screen-sm ">
        <NavLink to="/">
          <p className="text-2xl font-medium text-red-500 mx-5">HD MOVIES</p>
        </NavLink>
        <p className="font-medium text-3xl text-center py-5 text-gray-900">
          REGISTER
        </p>

        <div>
          <Form
            form={form}
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 12,
            }}
            name="register"
            onFinish={onFinish}
            initialValues={{
              residence: ["zhejiang", "hangzhou", "xihu"],
              prefix: "86",
            }}
            scrollToFirstError
          >
            <Form.Item
              label="Tài Khoản"
              name="taiKhoan"
              style={{ colors: "white" }}
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input placeholder="Nhập tài khoản" />
            </Form.Item>

            <Form.Item
              label="Mật Khẩu"
              name="matKhau"
              style={{ colors: "white" }}
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password placeholder="Nhập mật khẩu" />
            </Form.Item>

            <Form.Item
              name="comfirm"
              label="Xác nhận mật khẩu"
              dependencies={["matKhau"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("matKhau") === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      new Error(
                        "The two passwords that you entered do not match!"
                      )
                    );
                  },
                }),
              ]}
            >
              <Input.Password placeholder="Nhập mật khẩu" />
            </Form.Item>
            <Form.Item
              label="Số Điện Thoại"
              name="soDT"
              style={{ colors: "white" }}
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input placeholder="Nhập số đt" />
            </Form.Item>
            <Form.Item
              label="Mã nhóm"
              name="maNhom"
              style={{ colors: "white" }}
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input placeholder="Nhập mã Nhóm" />
            </Form.Item>
            <Form.Item
              name="hoTen"
              label="Họ và tên"
              rules={[
                {
                  required: true,
                  message: "Please input your name!",
                  whitespace: true,
                },
              ]}
            >
              <Input placeholder="Nhập họ tên" />
            </Form.Item>

            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
                {
                  required: true,
                  message: "Please input your E-mail!",
                },
              ]}
            >
              <Input placeholder="Nhập Email" />
            </Form.Item>
            <div className="flex justify-center">
              <button className="px-5 py-2 bg-blue-500 rounded text-white ">
                Đăng Ký
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
