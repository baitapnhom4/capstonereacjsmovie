import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../redux/actions/userAction";
import { localStorageSer } from "../services/localStoreageService";

export default function UserNav() {
  let dispatch = useDispatch();

  // userInform : lấy thông tin từ state redux truyền lên giao diện
  let userInform = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  // console.log(userInform);

  const handleLogout = () => {
    localStorageSer.user.remove();
    dispatch(loginAction(null));
  };
  let renderContent = () => {
    if (userInform) {
      return (
        <div className="flex space-x-5 items-center">
          <button>
            <p className="font-medium text-white">{userInform.hoTen}</p>
          </button>
          <button
            onClick={handleLogout}
            className="border-red-500 text-red-500 rounded px-5 py-3 border-2 font-medium "
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
      <div className=" flex space-x-5 items-center font-medium">
          <button
            onClick={() => {
              // click trở lại trang login
              window.location.href = "/login";
            }}
            className=" lg:px-4 lg:py-2 rounded border-blue-500 text-red-500 border-2 hover:bg-slate-900"
          >
            Đăng Nhập
          </button>
          <button
            onClick={() => {
              window.location.href = "/register";
            }}
            className="lg:px-4 lg:py-2 rounded border-green-500  text-red-500 border-2 hover:bg-slate-900"
          >
            Đăng Ký
          </button>
        </div>
      );
    }
  };
  return <div>{renderContent()}</div>;
}
