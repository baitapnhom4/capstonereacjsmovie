import React from "react";
import { NavLink } from "react-router-dom";

export default function NavMovie() {
  return (
    <div className="flex text-white md:text-sm lg:text-xl ">
      <NavLink to={"/lichchieu"} className="">
        <button className="sm:px-1 md:px-2 lg:px-6 xl:px-10 py-1 font-medium   hover:text-red-500  bg-blue-800 bg-opacity-30 rounded-full ">
          Lịch Chiếu
        </button>
      </NavLink>

      <button className=" sm:px-1 md:px-2 lg:px-6 xl:px-10 font-medium hover:text-red-500  ">
        Cụm Rạp
      </button>
      <button className="sm:px-1 md:px-2 lg:px-6 xl:px-10 font-medium hover:text-red-500  ">
        Tin Tức
      </button>
      <button className="sm:px-1 md:px-2 lg:px-6 xl:px-10 font-medium hover:text-red-500  ">
        Ứng Dụng
      </button>
    </div>
  );
}
