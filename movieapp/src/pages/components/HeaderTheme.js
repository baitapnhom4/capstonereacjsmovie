import React from "react";
import { NavLink } from "react-router-dom";
import NavMovie from "./NavMovie";
import UserNav from "./UserNav";

export default function HeaderTheme() {
  return (
    <div
      className=" px-5 sm:px-5 sm:h-14 md:px-5 md:h-14 lg:px-8  lg:h-20 justify-around flex items-center   bg-opacity-80 bg-black fixed z-10 w-full "
      // style={{ background: " #607d8b", opacity: 0.5 }}
    >
      <div className=" text-2xl font-medium   " style={{ color: "blue " }}>
        <NavLink to="/" className="flex  hover:text-purple-600">
          <img
            
            src="http://www.ci.vacaville.ca.us/Home/ShowPublishedImage/4259/636391714479630000"
            alt=""
            className=" w-10 sm:w-10 md:w-12 lg:w-12 lg:h-12 xl:w-14 my-auto "
          />
          <p className="mx-2 items-center  text-lg md:text-2xl xl:text-3xl  py-2 ">
            HD MOVIES
          </p>
        </NavLink>
      </div>
      <NavMovie />
      <UserNav />
    </div>
  );
}
