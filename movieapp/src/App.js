import "./App.css";
import LoginPage from "./pages/LoginPage/LoginPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout";
import HomePage from "./pages/HomePage/HomePage";
import RegisterPage from "./pages/RegisterPage/RegisterPage";

import DeTailPage from "./pages/DeTailPage/DeTailPage";
// import DatVePhim from "./pages/DatVePhim/DatVePhim";
import Checkout from "./pages/CheckoutPage/Checkout";
import TabMovie from "./pages/HomePage/TabMovie";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/lichchieu" element={<Layout Component={TabMovie} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DeTailPage} />}
          />
          {/* <Route path="/datve/:id" element={<Layout Component={DatVePhim} />} /> */}
          <Route
            path="/checkout/:id"
            element={<Layout Component={Checkout} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
